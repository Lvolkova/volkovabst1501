import java.util.Scanner;

//класс Palindrome, определяющий является ли строка палиндромом
public class Palindrome {
    //Основной класс, выводящий ответ
	public static void main(String[] args) {
	//Ввод строки 
		System.out.println("ВВедите строку: ");
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		//Разбивание строки на массив строк
		args = str.split("\\s");
		//Цикл проверяющий каждое слово: является палиндромом или нет
		for (int i=0; i <args.length; i++) {
			String s = args[i];
			//Вызов функции, определяющей палиндром
			if (isPalindrome(s))
			System.out.print(s +"Палиндром "+ "\n");
		else System.out.print(s+ "Не палиндром "+"\n");
		}	
		
	}
	//Функция, которая переворачивает слово
	public static String reverseString (String s){
		String s1="";
		//Цикл, проходящий от конца слова до его начала и создающий новое слово
		//только в обратном порядке
		for (int i = s.length()-1; i>=0;--i)
			s1+= s.charAt(i);
		return s1;
	
	}
	//Функция, сравнивающая исходное слово с впреобразованным
	public static boolean isPalindrome (String s) {
		if (s.equals(reverseString(s))) return true;
		else return false;
	}

}
